const express = require('express');
const routes = require('./routes/routes');
const cors = require('cors');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

require('dotenv').config();

const app = express();
app.use(cors());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(routes);

mongoose.connect(process.env.DB_STRING, 
                  { useNewUrlParser: true, useUnifiedTopology: true }
                );

app.listen(process.env.APP_PORT, () => console.log('Online Api - Port:' + process.env.APP_PORT));